# Projet Infrastructure
## Présentation

Vous devez réaliser une application réalisant la gestion de jeux vidéos.
L'infrastructure attendue doit être décomposée en deux machines virtuelles, une servant à gérer la base de données, l'autre servant d'application pour le Back Office.

```mermaid
flowchart LR
  subgraph Application
    direction LR
    subgraph BackOfficeServer
        direction RL
    end
    subgraph DatabaseServer
        direction LR
    end
  end
  BackOfficeServer <--> DatabaseServer
```

L'objectif étant de pouvoir réaliser un CRUD pour vos jeux vidéos.

## Application attendue

Votre application doit permettre de gérer des jeux vidéos. 
Un jeu vidéo ayant à minima : 
 - Un nom
 - Une image
 - Une maison édition
 - Une évaluation
 - La date d'évaluation

```mermaid
classDiagram
    class VideoGame{
      +String title
      +String brand
      +String image
      +int evaluation
      +date date_evaluation
    }
```

L'objectif pour vous étant de pouvoir lister vos jeux vidéos favoris avec votre propre évaluation sur le jeu. Ainsi réalisez un CRUD permettant de gérer les données. 

Un crud consiste à faire un Create, Read, Update et Delete.
Si vous réalisez un site web, les pages suivantes sont attendues : 
- index.php (liste des jeux videos)
- show.php?id=<id> (affichage d'un jeu)
- edit.php?id=<id> (modification d'un jeu)
- create.php (création d'un jeu)

La date d'évaluation sera automatiquement stockée lors de la création ou mise à jour de l'enregistrement.


Attention, lorsqu'on développe, il est important de bien structurer votre code.  Ainsi vous aurez très probablement un fichier `db.php` permettant de gérant la connnection, et ce fichier sera inclu dans tous les fichiers cités précédemment.

Idem, il est important de sortir les identifiants de connection de la base de données. 
Pour ce faire, il est d'usage de créer un fichier `.env`. Ce fichier n'est pas à tracker via git mais contiendra les informations nécessaires utilisées lors de la connection à la DB.

## Contraintes

Ce projet devra être réalisé en groupe de 2.
La base de données doit être sécurisée. Il n'est pas attendu à ce que le back office utilise directement le compte root. 
Et l'utilisateur connecté ne doit pouvoir se connecter que depuis l'ip du serveur du back office.

## Bareme
Un bonus sera apporté aux projets personnels et aux groupes qui se challengent en proposant des fonctionnalités plus poussées :
 - reverse proxy
 - connection ssh sur les serveurs
 - backup automatisé de la BDD
 - logs
 - load balancer pour la BDD
 - ...

## Livrable attendu

### Dépôt GIT (accessible par votre intervenant)

Ce dépôt devra contenir un fichier README expliquant : 
 - Le but de votre projet
 - Le cadre de développement de votre projet (école, votre niveau d'études, les containtes de délai et de groupe)
 - La stack technique choisie (Langage, framework, moteur de base de données)
 - Comment installer votre projet
 - Un screenshot de l'application finale

Ce dépôt devra également contenir : 
- le code source de votre application
- la documentation technique
- la structure de votre base de données

### Une documentation d’architecture
- l'infrastructure mise en place
- la communication réalisée entre l'application et la base de données
- les ports alloués ainsi que la sécurité mise en place

- détailler l’utilisation des outils/services mis en place
Exemple : pour la mise en place d’une sauvegarde automatisée
- comment sauvegarder un nouveau dossier ?
- comment restaurer à une date antérieure ?

## Barème
- **4pts** - Respect des consignes
- **6pts** - Propreté du code
  - Pas de code superflu
  - Organisation propre des fichiers 
  - Clareté du code
  - Fonctionnalités implémentées
- **2pts** - Base de données
- **4pts** - UI/UX de l'application
  - le site est responsive
  - esthétique de l'application
- **4pts** - Documentation d'architecture

